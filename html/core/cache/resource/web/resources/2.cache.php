<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 2,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'PricingEN',
    'longtitle' => '',
    'description' => '',
    'alias' => 'pricingen',
    'alias_visible' => 0,
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 2,
    'menuindex' => 1,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1580929427,
    'editedby' => 1,
    'editedon' => 1580929462,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1580929462,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'pricingen.html',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    '_content' => '<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Pricing</title>
      <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap&subset=latin-ext" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="css/main.css">
   </head>
   <body>
      <!----  Menu  ---->
      <header class="header">
         <div class="top_line text-right">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <ul class="top_info">
                        <li class="mail"><a href="mailto:info@webarbor.me">info@webarmor.me</a></li>
                        <li><a href="#">Ru</a></li>
                        <li><span>/</span></li>
                        <li><a href="#">En</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         	<div class="nav">
		<div class="container">
			<div class="row">
				
				<div class="col-12 col-sm-12 col-md-2 col-lg-2 logo">
					<img src="img/logowa.png" alt="WebArmor">
				</div>

				<div class="col-12 col-sm-12 col-md-5 col-lg-6">
					<ul class="menu">
						<li class="menu__link">
							<a href="http://localhost:8000/">Home</a>
						</li>
						<li class="menu__link">
							<a href="#">Features</a>
						</li>
						<li class="menu__link">
							<a href="pricingen.html">Pricing</a>
						</li>
						<li class="menu__link">
							<a href="blogen.html">Blog</a>
						</li>
					</ul>
				</div>
				
				<div class="col-12 col-sm-12 col-md-5 col-lg-4">
					<div class="button_group">
						<button type="button" class="btn btn__sign-in btn__top">
							SignIn
						</button>
						<button type="button" class="btn btn__get-started btn__top">
							Get started
						</button>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
      </header>


      <!----  Offer  ---->
      <section class="offer_pricing">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <h1>The Powerful Security without the High Cost</h1>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 text-center">
                  <p class="margin_top-40 font_size-22">Choose the plan that works for you <br>or get Free Trial</p>
               </div>
            </div>
         </div>
      </section>
      
      <section class="price margin_top-40 margin_bottom-100">
         <div class="container">
            <div class="row">
               <div class="col-12 col-sm-6 col-md-4">
                  <div class="card card-first">
                     <div class="card-body">
                        <h5 class="card-title text-center">Startup</h5>
                        <span class="cost text-center">$49</span>
                        <div class="text-center card_list">
                           <ul>
                              <li> 30 Mb/sec Legitimate traffic</li>
                              <li>
                                 1 Websites
                              </li>
                              <li>
                                 1 Upstream IP
                              </li>
                              <li>1 Free SSL certificate</li>
                           </ul>
                        </div>
                        <div class="text-center">
                           <a href="#" class="btn btn-primary">Purchase</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4">
                  <div class="card card-middle">
                     <div class="card-body">
                        <h5 class="card-title text-center">Business</h5>
                        <span class="cost text-center">$149</span>
                        <div class="text-center card_list_middle">
                           <ul>
                              <li> 150 Mb/sec Legitimate traffic</li>
                              <li>
                                 10 Websites
                              </li>
                              <li>
                                 1 Upstream IP
                              </li>
                              <li>SSL support</li>
                           </ul>
                        </div>
                        <div class="text-center">
                           <a href="#" class="btn btn-primary">Purchase</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-4">
                  <div class="card card-third">
                     <div class="card-body">
                        <h5 class="card-title text-center">Professional</h5>
                        <span class="cost text-center">$499</span>
                        <div class="text-center card_list">
                           <ul>
                              <li> 500 Mb/sec Legitimate traffic</li>
                              <li>
                                 50 Websites
                              </li>
                              <li>
                                 3 Upstream IP
                              </li>
                              <li>Round robin Load-balancing</li>
                              <li>SSL support</li>
                              <li>Whitelist</li>
                           </ul>
                        </div>
                        <div class="text-center">
                           <a href="#" class="btn btn-primary">Purchase</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <!----  Footer  ---->
      <section class="footer">
         <div class="container">
            <div class="row">
               <div class="col-6 col-md-3">
                  <div class="footer-block">
                     <p class="footer-block__title">
                        Product
                     </p>
                     <ul>
                        <li class="footer__item">
                           <a href="#">How it works</a>
                        </li>
                        <li class="footer__item">
                           <a href="#Features">Features</a>
                        </li>
                        <li class="footer__item">
                           <a href="#">Pricing</a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-6 col-md-3">
                  <div class="footer-block">
                     <p class="footer-block__title">
                        Help
                     </p>
                     <ul>
                        <li class="footer__item">
                           <a href="#">Docs</a>
                        </li>
                        <li class="footer__item">
                           <a href="#">Status page</a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-6 col-md-3">
                  <div class="footer-block">
                     <p class="footer-block__title">
                        Legacy
                     </p>
                     <ul>
                        <li class="footer__item">
                           <a href="#">Term of Use</a>
                        </li>
                        <li class="footer__item">
                           <a href="#">Privacy Policy</a>
                        </li>
                        <li class="footer__item">
                           <a href="#">Dispute resolution policy</a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-6 col-md-3">
                  <div class="footer-block">
                     <p class="footer-block__title">
                        About
                     </p>
                     <ul>
                        <li class="footer__item">
                           <a href="index.html">Home page</a>
                        </li>
                        <li class="footer__item">
                           <a href="#">Blog</a>
                        </li>
                        <li class="footer__item">
                           <a href="#">Contacts</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row footer__links d-flex justify-content-between">
               <div class="col-md-6">	
                  <span style="color:#52575C">@ 2020  </span> 
                  <a href="#">
                  WebArmor Technology Limited
                  </a>
               </div>
               <div class="col-md-6">	
                  <span style="color:#52575C">Website Defensed by  </span> 
                  <a href="#" style="color:#E89806">
                  WebArmor
                  </a>
               </div>
            </div>
         </div>
      </section>
      <script src="js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   </body>
</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[~1~]]' => 'http://localhost:8000/',
    '[[~2~]]' => 'pricingen.html',
    '[[~3~]]' => 'blogen.html',
    '[[$menuEN]]' => '	<div class="nav">
		<div class="container">
			<div class="row">
				
				<div class="col-12 col-sm-12 col-md-2 col-lg-2 logo">
					<img src="img/logowa.png" alt="WebArmor">
				</div>

				<div class="col-12 col-sm-12 col-md-5 col-lg-6">
					<ul class="menu">
						<li class="menu__link">
							<a href="http://localhost:8000/">Home</a>
						</li>
						<li class="menu__link">
							<a href="#">Features</a>
						</li>
						<li class="menu__link">
							<a href="pricingen.html">Pricing</a>
						</li>
						<li class="menu__link">
							<a href="blogen.html">Blog</a>
						</li>
					</ul>
				</div>
				
				<div class="col-12 col-sm-12 col-md-5 col-lg-4">
					<div class="button_group">
						<button type="button" class="btn btn__sign-in btn__top">
							SignIn
						</button>
						<button type="button" class="btn btn__get-started btn__top">
							Get started
						</button>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'menuEN' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'menuEN',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '	<div class="nav">
		<div class="container">
			<div class="row">
				
				<div class="col-12 col-sm-12 col-md-2 col-lg-2 logo">
					<img src="img/logowa.png" alt="WebArmor">
				</div>

				<div class="col-12 col-sm-12 col-md-5 col-lg-6">
					<ul class="menu">
						<li class="menu__link">
							<a href="[[~1~]]">Home</a>
						</li>
						<li class="menu__link">
							<a href="#">Features</a>
						</li>
						<li class="menu__link">
							<a href="[[~2~]]">Pricing</a>
						</li>
						<li class="menu__link">
							<a href="[[~3~]]">Blog</a>
						</li>
					</ul>
				</div>
				
				<div class="col-12 col-sm-12 col-md-5 col-lg-4">
					<div class="button_group">
						<button type="button" class="btn btn__sign-in btn__top">
							SignIn
						</button>
						<button type="button" class="btn btn__get-started btn__top">
							Get started
						</button>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '	<div class="nav">
		<div class="container">
			<div class="row">
				
				<div class="col-12 col-sm-12 col-md-2 col-lg-2 logo">
					<img src="img/logowa.png" alt="WebArmor">
				</div>

				<div class="col-12 col-sm-12 col-md-5 col-lg-6">
					<ul class="menu">
						<li class="menu__link">
							<a href="[[~1~]]">Home</a>
						</li>
						<li class="menu__link">
							<a href="#">Features</a>
						</li>
						<li class="menu__link">
							<a href="[[~2~]]">Pricing</a>
						</li>
						<li class="menu__link">
							<a href="[[~3~]]">Blog</a>
						</li>
					</ul>
				</div>
				
				<div class="col-12 col-sm-12 col-md-5 col-lg-4">
					<div class="button_group">
						<button type="button" class="btn btn__sign-in btn__top">
							SignIn
						</button>
						<button type="button" class="btn btn__get-started btn__top">
							Get started
						</button>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
    ),
    'modTemplateVar' => 
    array (
    ),
  ),
);