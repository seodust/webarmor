<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 5,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'HomeRU',
    'longtitle' => '',
    'description' => '',
    'alias' => 'homeru',
    'alias_visible' => 0,
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 4,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1580930105,
    'editedby' => 1,
    'editedon' => 1580930114,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1580930114,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'homeru.html',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    '_content' => '<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WebArmor</title>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap&subset=latin-ext" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

<!----  Menu  ---->
<header class="header">
	
		<div class="top_line text-right">
			<div class="container">
				<div class="row">
			<div class="col-md-12">
				<ul class="top_info">
					<li class="mail"><a href="mailto:info@webarbor.me">info@webarmor.me</a></li>
					<li><a href="#">Ru</a></li>
					<li><span>/</span></li>
					<li><a href="#">En</a></li>					
				</ul>				
			</div>
		</div>
			</div>
			
		</div>
		
	
	<div class="nav">
		<div class="container">
			<div class="row">
				
				<div class="col-12 col-sm-12 col-md-2 col-lg-2 logo">
					<img src="img/logowa.png" alt="WebArmor">
				</div>

				<div class="col-12 col-sm-12 col-md-5 col-lg-6">
					<ul class="menu">
						<li class="menu__link">
							<a href="#">Home</a>
						</li>
						<li class="menu__link">
							<a href="#">Features</a>
						</li>
						<li class="menu__link">
							<a href="#">Pricing</a>
						</li>
						<li class="menu__link">
							<a href="#">Blog</a>
						</li>
					</ul>
				</div>
				
				<div class="col-12 col-sm-12 col-md-5 col-lg-4">
					<div class="button_group">
						<button type="button" class="btn btn__sign-in btn__top">
							SignIn
						</button>
						<button type="button" class="btn btn__get-started btn__top">
							Get started
						</button>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</header>

 <!----  Offer  ---->
 <section class="offer">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
				<h1 class="offer__title">
					?????? ?? DDoS-????
				</h1>
				<p class="offer__text">
					??????? ???????????????? ?????? ?? DDoS-???? ?????? ?????????
				</p>
				<form action="">
					<div class="form_center">
					<input type="email" name="email" placeholder="?????? ??? email" class="form__input" required autocomplete="off" autocorrect="off" autocapitalize="off">
					<button type="button" class="btn btn__free-trial">
						Free trial
				</button>
					</div>
				
				</form>
				
				<p class="offer__free-trial">
					??????? ?????????? ?????? 14 ????
				</p>
			</div>
			

			
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<img src="img/offer_img.jpg" alt="" class="offer__img">
			</div>
			</div>
			
			
		
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h4 class="trusted__text title">
					??? ???????? ????? ????????????? ????????-????????
				</h4>
			</div>
		</div>

	</div>
</section>

 <!----  Why WebArmor  ---->
 <section class="properties">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h2 class="properties__title title">
					?????? ???????? ?????????
				</h2>
			</div>
		</div>
		
		<div class="row">
			<div class=" col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="properties__list d-flex">
					<img src="img/icon1.png" class="property__icon" 	alt="Easy to connect">
					<div class="property__block">
						<h4 class="property__title">
							??????? ???????????
						</h4>
						<p class="property__text">
							?????????? ???????? ?-?????? ? DNS
						</p>
					</div>
				</div>	
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="properties__list d-flex">
					<img src="img/icon2.png" class="property__icon" 	alt="Full-auto protection mode">
					<div class="property__block">
						<h4 class="property__title">
							?????????? ????????? ??????? ???????
						</h4>
						<p class="property__text">
							?????????? ?????? ?? ?????? L7 ?????? OSI
						</p>
					</div>
				</div>	
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="properties__list d-flex">
					<img src="img/icon3.png" class="property__icon" 	alt="Reaction time to a DDoS attack">
					<div class="property__block">
						<h4 class="property__title">
							??????? ??????? ?? ?????
						</h4>
						<p class="property__text">
							?? 30 ?????? ?? 3 ?????
						</p>
					</div>
				</div>	
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="properties__list d-flex">
					<img src="img/icon4.png" class="property__icon" 	alt="Minimum false-positive">
					<div class="property__block">
						<h4 class="property__title">
							??????? ?????-????????????? ????????????
						</h4>
						<p class="property__text">
							? ????? 0%, ?? ????? ????? ?? ????? 5%
						</p>
					</div>
				</div>	
			</div>
		</div>

		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="properties__list d-flex">
					<img src="img/icon5.png" class="property__icon" 	alt="No CAPTCHA">
					<div class="property__block">
						<h4 class="property__title">
							??? ????? !
						</h4>
						<p class="property__text">
							? ?????-???? ?????? ????????, ???????????? ??????????? ?????? ????? 
						</p>
					</div>
				</div>	
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="properties__list d-flex">
					<img src="img/icon6.png" class="property__icon" 	alt="Support">
					<div class="property__block">
						<h4 class="property__title">
							???????
						</h4>
						<p class="property__text">
							?? ?????? ?????? ?????? ??? ? ???????? ?? ???? ???????
						</p>
					</div>
				</div>	
			</div>
		</div>	
	</div>
</section>

<!----  Features  ---->
<section class="features" id="Features">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="features__title title">
					???? ?????? ??????? ? ???, ????? ???????????? ??????????? ??????? ??? ?????? ???-??????????
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<img src="img/filtering.png" alt="HTTPS filtering" class="feature__img">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="feature__block">
					<h3 class="feature__title">
					HTTPS ??????????
				</h3>
				<p class="feature__text">
					HTTPS-?????? ???????????????? ? ???? WebArmor. ????? ??????? ? ??????? ?????? ????? ????????? ? ???????????? ?? ??? ????.
				</p>
				</div>
				

			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-sm-first order-last">
				<div class="feature__block">
					<h3 class="feature__title">
					???????????? ???????
				</h3>
				<p class="feature__text">
					?? ????????? Round-robin - ??????? ?????????? ?????????????? ????? ?????????, ??????? ?????? ??????????? ?? ??????? ?? ???????? ?????????? ???????.
				</p>
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-sm-last order-first">
				<img src="img/balancing.png" alt="HTTPS filtering" class="feature__img">
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<img src="img/freessl.png" alt="HTTPS filtering" class="feature__img">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="feature__block">
					<h3 class="feature__title">
					?????????? SSL ??????????
				</h3>
				<p class="feature__text">
					???????? ????????????? ??????????, ????????????? ?????????? SSL ??????????.
				</p>
				</div>
				
			</div>
		</div>

</section>


<!----  Screen 4    Call to Action ---->
<section class="cta">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="cta__title title">
					??????? ???????????? ?????? ????? ? ????????
				</h3>
				<p class="cta__text title">
					14 ???? ?????????? ???????? ??????. ??????? ???????????.
				</p>
				<center>
					<form action="">

						<div class="cta__form">
						<input type="email" name="email" placeholder="??????? ??? email" class="form__input" required autocomplete="off" autocorrect="off" autocapitalize="off">
						<button type="button" class="btn btn__free-trial">
								Free trial
						</button>
					</div>
					</form>
					
				</center>
			</div>
		</div>
	</div>
</section>

<!----  Footer  ---->

<section class="footer">
	<div class="container">

		<div class="row">
			<div class="col-6 col-md-3">
				<div class="footer-block">
					<p class="footer-block__title">
						Product
					</p>
					<ul>
						<li class="footer__item">
							<a href="#">How it works</a>
						</li>
						<li class="footer__item">
							<a href="#Features">Features</a>
						</li>
						<li class="footer__item">
							<a href="#">Pricing</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="col-6 col-md-3">
				<div class="footer-block">
				<p class="footer-block__title">
					Help
				</p>
				<ul>
					<li class="footer__item">
						<a href="#">Docs</a>
					</li>
					<li class="footer__item">
						<a href="#">Status page</a>
					</li>
				</ul>
			</div>
			</div>
			
			<div class="col-6 col-md-3">
				<div class="footer-block">
				<p class="footer-block__title">
					Legacy
				</p>
				<ul>
					<li class="footer__item">
						<a href="#">Term of Use</a>
					</li>
					<li class="footer__item">
						<a href="#">Privacy Policy</a>
					</li>
					<li class="footer__item">
						<a href="#">Dispute resolution policy</a>
					</li>
				</ul>
			</div>
			</div>
			
			<div class="col-6 col-md-3">
				<div class="footer-block">
				<p class="footer-block__title">
					About
				</p>
				<ul>
					<li class="footer__item">
						<a href="http://localhost:8000/">Home page</a>
					</li>
					<li class="footer__item">
						<a href="#">Blog</a>
					</li>
					<li class="footer__item">
						<a href="#">Contacts</a>
					</li>
				</ul>
			</div>
			</div>
			

		</div>

		<div class="row footer__links d-flex justify-content-between">
			<div class="col-md-6">	
				<span style="color:#52575C">@ 2020  </span> 
				<a href="#">
					WebArmor Technology Limited
				</a>
			</div>

			<div class="col-md-6">	
				<span style="color:#52575C">Website Defensed by  </span> 
				<a href="#" style="color:#E89806">
					WebArmor
				</a>
			</div>
				
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</section>
<script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

</body>
</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[~1~]]' => 'http://localhost:8000/',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
    ),
    'modSnippet' => 
    array (
    ),
    'modTemplateVar' => 
    array (
    ),
  ),
);